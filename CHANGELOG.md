# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.1] - 2024-02-12
### Fixed
- Apache alias domain parsing

## [2.0.0] - 2023-07-03
### Added
- Golang implementation of the tool

## [1.2.0] - 2023-06-29
### Added
- Configuration with ignore list marking items as ignored

## [1.1.0] - 2023-06-28
### Added
- Version file to s3 bucket

## [1.0.0] - 2023-06-27
### Added
- Initial release
