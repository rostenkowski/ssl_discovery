<?php
// error_reporting( error_reporting() & ~E_NOTICE );
$results = array();
 
function apache_vhosts(&$results) {	
	$binary='sudo /usr/sbin/apache2ctl';
	$command = " -S 2>&1 | grep 'port ' | awk {'print $2,$4'} | grep -v 'localhost'";
	$vhosts = shell_exec(sprintf("%s %s", $binary, $command));
	$vhosts = explode("\n", trim($vhosts));
 
	foreach($vhosts as $vhost) {
		$x = explode(' ', $vhost, 2);
		$new_entry['{#SERVERNAME}'] = $x[1];
		$new_entry['{#SERVERPORT}'] = $x[0];		
		if ( $x[0] == 80 ) {
			$new_entry['{#SERVERPROTO}'] = "http";
		} elseif ( $x[0] == 443 ) {
			$new_entry['{#SERVERPROTO}'] = "https";
		}
		if ( !empty($x[1]) || !empty($x[0]) ) {
			$results['data'][] = $new_entry;
		}
	}  
}
function nginx_vhosts(&$results) {
	$conts = shell_exec("awk '/server *{/{c=1; print;next} c&&/{/{c++} c&&/}/{c--} c' /etc/nginx/sites-enabled/*");
	foreach (explode("server ", $conts) as $cont){
		preg_match("/\b(server_name)\b(.*);/", $cont, $matches);
		if(isset($matches[2])){
			foreach(explode(" ", $matches[2]) as $domain){
				if($domain != "_" && $domain != ""){
					$result = array();
					$result['{#SERVERNAME}'] = $domain;
					$port = 80;
					preg_match("/listen.*\b([0-9]+)\b.*;/", $cont, $matc);
					if (isset($matc[1]))
						$port = $matc[1];
 
					if(preg_match("/(listen.*ssl)/", $cont, $listn)){
						$result['{#SERVERPORT}'] = $port;
                       	        		$result['{#SERVERPROTO}'] = "https";
					}else{
						$result['{#SERVERPORT}'] = $port;
                       	        		$result['{#SERVERPROTO}'] = "http";
					}
					$results['data'][] = $result;
				}
			}
		}
	}
}
 
function dovecot_imap_993(&$results) {
	$result = array();
	$server_hostname = rtrim(shell_exec("hostname"));
	$imap_listen = !intval(shell_exec("netstat -nta | grep ':993' >> /dev/null 2>&1 && echo $?"));
	if ( $imap_listen == 1 ) {
		$result['{#SERVERNAME}'] = $server_hostname;
		$result['{#SERVERPORT}'] = 993;
		$result['{#SERVERPROTO}'] = "imap";
		$results['data'][] = $result;
	}
}
 
function postfix_smtp_25(&$results) {
	$result = array();
	$server_hostname = rtrim(shell_exec("hostname"));
	$smtp_listen = !intval(shell_exec("netstat -nta | grep ':25' >> /dev/null 2>&1 && echo $?"));	
	$result['{#SERVERNAME}'] = $server_hostname;
	$result['{#SERVERPORT}'] = 25;
	$result['{#SERVERPROTO}'] = "smtp";
	$results['data'][] = $result; 
}
 
 
if (file_exists('/usr/sbin/apachectl')) {
	//echo json_encode( apache_vhosts($results) );
	apache_vhosts($results);
}
 
if (file_exists('/etc/nginx')) {
	nginx_vhosts($results);
 
}
 
if (file_exists('/etc/dovecot')) {
	dovecot_imap_993($results);
}
 
if (file_exists('/etc/postfix')) {
	postfix_smtp_25($results);
}
 
echo json_encode($results);
return 0;
?>

