package main

import (
	"bytes"
	"os/exec"
	"regexp"
	"strings"
)

func removeAllHashComments(input string) string {
	var output strings.Builder
	commentDetected := false

	for _, char := range input {
		if char == '#' {
			commentDetected = true
		} else if (char == '\n' || char == '\r') && commentDetected {
			output.WriteRune(char)
			commentDetected = false
		} else if !commentDetected {
			output.WriteRune(char)
		}
	}

	return output.String()
}

func getPortFromServerBlock(serverBlock string) string {
	rgx := regexp.MustCompile(`listen\s*([0-9]+)`)
	matches := rgx.FindStringSubmatch(serverBlock)

	if len(matches) > 1 {
		return matches[1]
	}

	return "80"
}

func getOnlyBlock(input string, start int) string {
	depth := 0
	blockOpened := false
	var output strings.Builder

	for i := start; i < len(input); i++ {
		if input[i] == '{' {
			start = i
			depth++
			blockOpened = true
		} else if input[i] == '}' {
			depth--
		}

		if depth == 0 && blockOpened {
			output.WriteString(string(input[i]))
			break
		}
		if blockOpened {
			output.WriteString(string(input[i]))
		}
	}

	return output.String()
}

func getServerNamesFromServerBlock(serverBlock string) []string {
	rgx := regexp.MustCompile(`server_name\s*([_0-9a-zA-Z\-\.\*\s]+)`)
	matches := rgx.FindStringSubmatch(serverBlock)

	var results []string

	if len(matches) > 1 {
		names := strings.Fields(matches[1])
		for _, name := range names {
			name = strings.TrimSpace(name)
			if name != "" && !contains(results, name) {
				results = append(results, name)
			}
		}
	}

	return results
}

func checkNginx() ([]ServerData, error) {
	cmd := exec.Command("/usr/sbin/nginx", "-T")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return nil, err
	}

	config := removeAllHashComments(out.String())
	rgx := regexp.MustCompile(`server\s`)
	matches := rgx.FindAllStringIndex(config, -1)
	dataArray := make([]ServerData, 0)

	for _, match := range matches {
		serverBlock := getOnlyBlock(config, match[0])
		port := getPortFromServerBlock(serverBlock)
		serverNames := getServerNamesFromServerBlock(serverBlock)

		for _, serverName := range serverNames {
			if serverName != "_" {
				data := ServerData{}
				if strings.HasPrefix(serverName, "*.") {
					data.ServerName = strings.Replace(serverName, "*", "WILDCARD-DOMAIN", 1)
				} else {
					data.ServerName = serverName
				}

				data.ServerPort = port
				if port == "80" {
					data.ServerProto = "http"
				} else {
					data.ServerProto = "https"
				}
				data.ServerIgnore = checkIgnore(serverName, data.ServerProto)

				dataArray = append(dataArray, data)
			}
		}
	}

	return dataArray, nil
}
