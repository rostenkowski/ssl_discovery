package main

import (
	"fmt"
	"os"
)

func checkMongodbPorts() ([]ServerData, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	ports := []int{27017, 27018, 27019}
	var dataArr []ServerData

	for _, port := range ports {
		if portIsOpen(hostname, port) {
			dataArr = append(dataArr, ServerData{ServerName: hostname, ServerPort: fmt.Sprintf("%d", port), ServerProto: "mongodb", ServerIgnore: checkIgnore(hostname, "mongodb")})
		}
	}
	return dataArr, nil
}
