package main

import "fmt"

func checkProxmox() ([]ServerData, error) {
	hostname, err := getHostname()
	if err != nil {
		return nil, err
	}

	port := 8006
	dataArray := make([]ServerData, 0)

	if portIsOpen(hostname, port) {
		serverData := ServerData{
			ServerName:   hostname,
			ServerPort:   fmt.Sprintf("%d", port),
			ServerProto:  "https",
			ServerIgnore: checkIgnore(hostname, "https"),
		}
		dataArray = append(dataArray, serverData)
	}

	return dataArray, nil
}
