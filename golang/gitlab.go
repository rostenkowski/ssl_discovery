package main

import (
	"bytes"
	"os/exec"
	"regexp"
)

func checkGitlab() (ServerData, error) {
	var serverData ServerData

	cmd := exec.Command("/usr/bin/gitlab-ctl", "show-config")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return serverData, err
	}

	rgx := regexp.MustCompile(`"external-url":\s"(http|https)?://([a-zA-Z\.0-9\-]*)`)
	match := rgx.FindStringSubmatch(out.String())

	if len(match) > 2 {
		serverData.ServerName = match[2]
		serverData.ServerPort = "80"
		if match[1] == "https" {
			serverData.ServerPort = "443"
		}
		serverData.ServerProto = match[1]
		serverData.ServerIgnore = checkIgnore(match[2], match[1])
	}

	return serverData, nil
}
